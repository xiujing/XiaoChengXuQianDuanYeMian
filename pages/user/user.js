// pages/user/user.js
var app = getApp()
Page( {
  data: {
    isSystem:0,
    userInfo: {},
    orderInfo:{},
    loadingText: '加载中...',
    loadingHidden: false,
  },
  onLoad: function () {
      var that = this
      //调用应用实例的方法获取全局数据
      app.getUserInfo(function(userInfo){
        //更新数据
        that.setData({
          userInfo:userInfo,
          loadingHidden: true
        })
      });
      wx.request({
        url: app.d.ceshiUrl + '/Api/User/is_system',
        data: { openid: app.globalData.userInfo.openid },
        header: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        success: function (res) {
          var system = 0;
          if (parseInt(res.data)) {
            system = 1;
          } else {
            system = 0;
          }
          that.setData({
            isSystem: system
          })
        }
      });
      this.loadOrderStatus();
  },
  onShow:function(){
    this.loadOrderStatus();
  },
  loadOrderStatus:function(){
    //获取用户订单数据
    var that = this;
    wx.request({
      url: app.d.ceshiUrl + '/Api/User/getorder',
      method:'post',
      data: {
        userId:app.d.userId,
      },
      header: {
        'Content-Type':  'application/x-www-form-urlencoded'
      },
      success: function (res) {
        //--init data        
        var status = res.data.status;
        if(status==1){
          var orderInfo = res.data.orderInfo;
          that.setData({
            orderInfo: orderInfo
          });
        }else{
          wx.showToast({
            title: '非法操作.',
            duration: 2000
          });
        }
      },
      error:function(e){
        wx.showToast({
          title: '网络异常！',
          duration: 2000
        });
      }
    });
  },
  scanQrcode:function(){
    wx.scanCode({
      success: function(res){
        var ordeSn = res.result;
        wx.navigateTo({
          url: '/pages/user/detail?orderId='+res.result
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '请检查二维码是否正确',
          icon: 'success',
          duration: 2000
        })
      }
    })
  }
})