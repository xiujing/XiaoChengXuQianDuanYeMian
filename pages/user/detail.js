var app = getApp();
// pages/order/detail.js
Page({
  data:{
    orderId:0,
    orderData:{},
    proData:[],
  },
  onLoad:function(options){
    this.setData({
      orderId: options.orderId,
    })
    this.loadProductDetail();
  },
  loadProductDetail:function(){
    var that = this;
    wx.request({
      url: app.d.ceshiUrl + '/Api/Order/order_details',
      method:'post',
      data: {
        order_id: that.data.orderId,
      },
      header: {
        'Content-Type':  'application/x-www-form-urlencoded'
      },
      success: function (res) {
        var status = res.data.status;
        if(status==1){
          var pro = res.data.pro;
          var ord = res.data.ord;
          that.setData({
            orderQrcode: app.d.ceshiUrl + '/Api/Order/orderQrcode?code=' + ord.order_sn,
            orderData: ord,
            proData:pro
          });
        }else{
          wx.showToast({
            title: res.data.err,
            duration: 2000
          });
        }
      },
      fail: function () {
          // fail
          wx.showToast({
            title: '网络异常！',
            duration: 2000
          });
      }
    });
  },
  setStatus:function(res){
    var _=this;
    wx.showModal({
      title: '确认核销',
      content: '确认核销该订单吗？',
      success: function (res) {
        if (res.confirm) {
          wx.request({
            url: app.d.ceshiUrl + '/Api/Order/set_order_status',
            data: { id: _.data.orderId },
            success: function (res) {
              var data=res.data;
              wx.showToast({
                title: data.msg,
                icon: 'success',
                duration: 2000,
                complete:function(){
                  if(data.code){
                    wx.navigateBack();
                  }
                }
              })
            },
            fail: function () {
              wx.showToast({
                title: '网络不通',
                icon: 'success',
                duration: 2000
              })
            }
          })
        }
      }
    })
      
  }
})