var app = getApp();
// pages/user/shoucang.js
Page({
  data:{
    page:1,
    productData:[],
  },
  onLoad:function(options){
    this.loadProductData();
  },
  onShow:function(){
    // 页面显示
    this.loadProductData();
  },
  removeFavorites:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;

    wx.showModal({
      title: '提示',
      content: '你确认移除吗',
      success: function(res) {
        res.confirm && wx.request({
          url: app.d.hostUrl + '/Api/User/collection_qu',
          method:'post',
          data: {
            id: id,
          },
          header: {
            'Content-Type':  'application/x-www-form-urlencoded'
          },
          success: function (res) {
            //--init data
            var data = res.data;
            //todo
            if(data.status == 1){
              that.loadProductData();
            }
          },
        });

      }
    });
  },
  loadProductData:function(){
    var that = this;
    wx.request({
      url: app.d.ceshiUrl + '/Api/User/collection',
      method:'post',
      data: {
        userId: app.d.userId,
        pageindex: that.data.page,
        pagesize:100,
      },
      header: {
        'Content-Type':  'application/x-www-form-urlencoded'
      },
      success: function (res) {
        console.log(res);
        //--init data
        var sc_list = res.data.sc_list;
        that.setData({
          productData: sc_list,
        });
        //endInitData
      },
    });
  }
});