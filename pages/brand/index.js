var app = getApp()
Page({
    data: {
      page:1,
      load:true,
      brandList:[]
    },
    onLoad: function(options) {
      this.getBrand();
    },
    onReachBottom:function(){
      this.getBrand();
    },
    getBrand:function(){
      var _=this;
      if(!this.data.load){
        wx.showToast({
          title: '没有更多数据',
          icon: 'success',
          duration: 2000
        });
        return false;
      }
      wx.request({
        url: app.d.ceshiUrl +'/Api/Index/get_brand',
        method:'POST',
        data: { page: _.data.page },
        header: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        success:function(res){
          var brandList = _.data.brandList;
          var page=_.data.page;
          var load=_.data.load;
          console.log(res.data);
          if (!res.data.length){
            load=false;
          }
          page++;
          brandList=brandList.concat(res.data);
          _.setData({
            load:load,
            page:page,
            brandList:brandList
          })
        },
        fail:function(){
          wx.showToast({
            title: '网络错误',
            icon: 'success',
            duration: 2000
          })
        }
      })
    }

})